// import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:mariage_app/infrastructure/api/rest_api_interceptor.dart';
import 'package:mariage_app/presentation/core/styles/theme_datas.dart';
import 'package:mariage_app/presentation/core/translations/localization_service.dart';
import 'package:mariage_app/presentation/navigation/navigation.dart';
import 'package:mariage_app/presentation/navigation/routes.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'infrastructure/api/rest_api_client.dart';

// import 'package:flutter_localizations/flutter_localizations.dart';

void main() async {
  await GetStorage.init();
  WidgetsFlutterBinding.ensureInitialized();

  if (kIsWeb) {
    await Firebase.initializeApp(
      options: FirebaseOptions(
        apiKey: "AIzaSyBvtmIIiGDXyC-vIpJYi50JZImNHidCoAk",
        appId: "1:311534693314:web:ee215728a7e7468319845b",
        messagingSenderId: "311534693314",
        projectId: "mariageapp-7fd32",
        storageBucket: "mariageapp-7fd32.appspot.com",
      ),
    );
  } else {
    await Firebase.initializeApp(
      name: "AndroidApp",
      options: FirebaseOptions(
        apiKey: "AIzaSyBvtmIIiGDXyC-vIpJYi50JZImNHidCoAk",
        appId: "1:311534693314:android:8cded3e1da76a0a619845b",
        messagingSenderId: "311534693314",
        projectId: "mariageapp-7fd32",
        storageBucket: "mariageapp-7fd32.appspot.com",
      ),
    );
  }

  await initServices();
  final box = GetStorage();
  String initialRoute = Routes.LOGIN;

  String nomPrenom = box.read("nomPrenom") ?? "";

  debugPrint("name user: " + nomPrenom.toString());

  if (kIsWeb) {
    initialRoute = Routes.EDIT;
  } else if (nomPrenom.isNotEmpty) {
    initialRoute = Routes.CAMERASCREEN;
  }

  runApp(
    GetMaterialApp(
      title: 'MariageApp',
      initialRoute: initialRoute,
      getPages: Nav.routes,
      theme: XThemeData.light(),
      darkTheme: XThemeData.dark(),
      themeMode: ThemeMode.light,
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      locale: Locale('fr', 'FR'),
      supportedLocales: const <Locale>[
        Locale('fr', 'FR'),
        Locale('en', 'US'),
      ],
      fallbackLocale: Locale('fr', 'FR'),
      translations: LocalizationService(),
    ),
  );
}

Future<void> initServices() async {
  /// Here is where you put get_storage, hive, shared_pref initialization.
  /// or moor connection, or whatever that's async.

  // await Firebase.initializeApp();
  await GetStorage.init();

  Get.put(
    RestApiClient(
      restApiInterceptor: Get.put(
        RestApiInterceptor(),
      ),
    ),
    permanent: true,
  );

  print('All services started !');
}
