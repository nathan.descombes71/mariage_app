import 'package:flutter/foundation.dart';

class Routes {
  static String get initialRoute {
    // TODO: implement method
    return CAMERASCREEN;
  }

  static const LOGIN = '/login';
  static const CAMERASCREEN = '/camera-screen';
  static const EDIT = '/edit';
}
