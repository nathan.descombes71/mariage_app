import 'package:mariage_app/presentation/views/camera_screen/camera_screen_view.dart';
import 'package:mariage_app/presentation/views/edit/edit_view.dart';
import 'package:mariage_app/presentation/views/edit/edit_view_controller_bindings.dart';
import 'package:mariage_app/presentation/views/login/login_view.dart';
import 'package:mariage_app/presentation/views/login/login_view_controller_bindings.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import '../views/camera_screen/camera_screen_view_controller_bindings.dart';
import 'routes.dart';

class Nav {
  static List<GetPage> routes = [
    /// REVIEW [Mobile & Web] routes
    GetPage(
      name: Routes.LOGIN,
      page: () => LoginView(),
      binding: LoginViewControllerBindings(),
    ),
    GetPage(
      name: Routes.EDIT,
      page: () => EditView(),
      binding: EditViewControllerBindings(),
    ),
    GetPage(
      name: Routes.CAMERASCREEN,
      page: () => CameraScreenView(),
      binding: CameraScreenViewControllerBindings(),
    ),
  ];
}
