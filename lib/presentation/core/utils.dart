import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';

ButtonStyle buttonStyleApp() {
  return ButtonStyle(
      backgroundColor:
          MaterialStateProperty.all<Color>(Get.theme.colorScheme.primary),
      foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
      shape: MaterialStateProperty.all<OutlinedBorder>(
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0))));
}

toast(BuildContext context, String message,
    {bool success = false, bool error = false}) {
  // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(message)));

  if (success) {
    showTopSnackBar(
      context,
      CustomSnackBar.success(
        message: message,
        messagePadding: const EdgeInsets.symmetric(horizontal: 12),
        icon:
            const Icon(Icons.info_outline, color: Color(0x15000000), size: 120),
        backgroundColor: Get.theme.colorScheme.primary,
      ),
    );
  } else if (error) {
    showTopSnackBar(
      context,
      CustomSnackBar.error(
          message: message,
          messagePadding: const EdgeInsets.symmetric(horizontal: 12),
          icon: const Icon(Icons.info_outline,
              color: Color(0x15000000), size: 120)),
    );
  } else {
    showTopSnackBar(
      context,
      CustomSnackBar.info(
          message: message,
          messagePadding: const EdgeInsets.symmetric(horizontal: 12),
          icon: const Icon(Icons.info_outline,
              color: Color(0x15000000), size: 120),
          backgroundColor: Colors.black),
    );
  }
}
