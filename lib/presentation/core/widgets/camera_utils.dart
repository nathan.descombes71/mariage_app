import 'package:camera/camera.dart';
import 'package:flutter/widgets.dart';

class CameraFlashState {
  // Define a state of flash mode of the camera
  final IconData icon;
  final String text;
  final FlashMode flashMode;

  CameraFlashState(this.icon, this.text, this.flashMode);
}

class RotateCameraStateManager {
  // Used to switch between camera flash states
  late CameraFlashState _currentCameraFlashState;
  late List<CameraFlashState> cameraFlashStates;

  RotateCameraStateManager(this.cameraFlashStates) {
    _currentCameraFlashState = cameraFlashStates[0];
  }

  CameraFlashState next() {
    int index = cameraFlashStates.indexOf(_currentCameraFlashState);
    if (index == cameraFlashStates.length - 1) {
      _currentCameraFlashState = cameraFlashStates.first;
    } else {
      _currentCameraFlashState = cameraFlashStates[index + 1];
    }

    return _currentCameraFlashState;
  }

  CameraFlashState current() {
    return _currentCameraFlashState;
  }
}
