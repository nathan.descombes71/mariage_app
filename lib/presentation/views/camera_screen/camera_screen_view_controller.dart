import 'dart:io';
import 'dart:math';

import 'package:camera/camera.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:ionicons/ionicons.dart';
import 'package:mariage_app/presentation/core/utils.dart';
import 'package:mariage_app/presentation/core/widgets/camera_utils.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:uuid/uuid.dart';

import '../../core/widgets/circular_icon_button.dart';
import 'component/dialog_post_photo.dart';

class CameraScreenViewController extends GetxController with StateMixin {
  CameraScreenViewController();

  late CameraController cameraController;
  RxBool toggleCamera = false.obs;
  late final RotateCameraStateManager rotateCameraStateManager;
  late String imagePath;
  final picker = ImagePicker();
  List<CameraDescription> cameras = [];
  final box = GetStorage();

  final RxInt currentIndex = 0.obs;

  String nameUser = "anonyme";

  double minAvailableZoom = 1.0;
  double maxAvailableZoom = 1.0;
  RxDouble currentZoomLevel = 1.0.obs;

  late final List<CameraFlashState>
      flashStates; // late final RotateCameraStateManager rotateCameraStateManager;

  @override
  Future<void> onInit() async {
    change(null, status: RxStatus.loading());

    nameUser = box.read("nomPrenom");

    FirebaseAuth mAuth = FirebaseAuth.instance;

    await mAuth.signInAnonymously();

    flashStates = [
      CameraFlashState(Icons.flash_off, 'Off', FlashMode.off),
      CameraFlashState(Icons.flash_on, 'On', FlashMode.torch),
    ];

    rotateCameraStateManager = RotateCameraStateManager(flashStates);

    cameras = await getCameras();

    try {
      await onCameraSelected(cameras[0]);
    } catch (e) {
      await getGalleryImage();
    }

    super.onInit();
  }

  Future<void> captureImage() async {
    await takePicture().then((xfile) async {
      var uuid = Uuid();

      Reference ref = FirebaseStorage.instance
          .ref()
          .child((xfile?.name ?? uuid.v4() + "png"));

      imagePath = xfile!.path;

      await cameraController.setFlashMode(FlashMode.off);

      File? fileToSend;

      fileToSend = File(xfile.path);

      Get.dialog(DialogPostPhoto(
        imagePath: imagePath,
        onDownload: () {},
        onShare: () {},
        onSend: () {
          Uuid uuid = Uuid();

          final metadata = SettableMetadata(
            customMetadata: {
              'picked-file-path': fileToSend!.path,
              "user": nameUser,
              "id": uuid.v4()
            },
          );
          ref.putFile(fileToSend, metadata).then((p0) async {
            Get.back();
            Get.snackbar(
              '',
              '',
              backgroundColor: Colors.transparent,
              titleText: CustomSnackBar.success(
                message: "Image envoyé avec succès",
                messagePadding: const EdgeInsets.symmetric(horizontal: 12),
                icon: const Icon(Icons.info_outline,
                    color: Color(0x15000000), size: 120),
                backgroundColor: Get.theme.colorScheme.primary,
              ),
            );
          }).catchError((error) {
            print(error);
          });
        },
      ));
    });
  }

  Future<XFile?> takePicture() async {
    if (!cameraController.value.isInitialized) {
      return null;
    }

    if (cameraController.value.isTakingPicture) {
      return null;
    }

    try {
      if (rotateCameraStateManager.current().flashMode == FlashMode.torch) {
        cameraController.setFlashMode(FlashMode.torch);
      }

      XFile filePath = await cameraController.takePicture();

      return filePath;
    } on CameraException catch (e) {
      print(e);
      return null;
    }
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    cameraController.dispose();
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  Future<void> onCameraSelected(CameraDescription cameraDescription) async {
    cameraController =
        CameraController(cameraDescription, ResolutionPreset.max);

    cameraController.addListener(() {
      if (cameraController.value.hasError) {
        //TODO - ToastErreur
        print(cameraController.value.errorDescription);
        // toast(context, cameraErrorText, error: true);
      }
    });

    try {
      await cameraController.initialize();

      cameraController
          .getMaxZoomLevel()
          .then((value) => maxAvailableZoom = value);
      cameraController
          .getMinZoomLevel()
          .then((value) => minAvailableZoom = value);

      // Reset zoom
      currentZoomLevel.value = 1.0;
      await cameraController.setZoomLevel(currentZoomLevel.value);
    } on CameraException catch (e) {
      // If we don't have permission to access camera
      // Open the album picker
      await getGalleryImage();
    }

    cameraController.setFlashMode(rotateCameraStateManager.current().flashMode);
  }

  Future<void> getGalleryImage() async {
    try {
      var uuid = Uuid();
      List<XFile>? pickedFile = await picker.pickMultiImage();

      for (var i = 0; i < (pickedFile ?? []).length; i++) {
        File? fileToSend;

        Reference ref = FirebaseStorage.instance
            .ref()
            .child((pickedFile?[i].name ?? "image"));

        if (pickedFile != null) {
          fileToSend = File(pickedFile[i].path);
        }

        if (fileToSend != null) {
          await Get.dialog(DialogPostPhoto(
            imagePath: fileToSend.path,
            onDownload: () {},
            onShare: () {},
            onSend: () {
              final metadata = SettableMetadata(
                customMetadata: {
                  'picked-file-path': fileToSend!.path,
                  "user": nameUser,
                  "id": uuid.v4()
                },
              );
              ref.putFile(fileToSend, metadata).then((p0) async {
                Get.back();
                Get.snackbar(
                  '',
                  '',
                  backgroundColor: Colors.transparent,
                  titleText: CustomSnackBar.success(
                    message: "Image envoyé avec succès",
                    messagePadding: const EdgeInsets.symmetric(horizontal: 12),
                    icon: const Icon(Icons.info_outline,
                        color: Color(0x15000000), size: 120),
                    backgroundColor: Get.theme.colorScheme.primary,
                  ),
                );
              }).catchError((error) {
                print(error);
              });
            },
          ));

          // ref.putFile(fileToSend, metadata).catchError((error) {
          //   print(error);
          // });
        }
      }
    } catch (e) {
      print(e);
    }
  }

  Future<List<CameraDescription>> getCameras() async {
    try {
      print(await availableCameras());
      return await availableCameras();
    } on CameraException catch (e) {
      print(e.description);
      rethrow;
    }
  }
}
