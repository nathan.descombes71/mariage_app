import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';

import '../../../core/widgets/circular_icon_button.dart';

class DialogPostPhoto extends StatefulWidget {
  final Function() onSend;
  final Function() onDownload;
  final Function() onShare;
  final String imagePath;

  const DialogPostPhoto({
    required this.imagePath,
    required this.onSend,
    required this.onDownload,
    required this.onShare,
  });

  @override
  State<DialogPostPhoto> createState() => _DialogPostPhotoState();
}

class _DialogPostPhotoState extends State<DialogPostPhoto> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Container(
          width: Get.width,
          height: Get.height,
          child: Column(
            children: [
              Expanded(
                child: Container(
                  color: Get.theme.colorScheme.primary,
                  child: Stack(
                    fit: StackFit.expand,
                    children: [
                      Image.file(
                        File(widget.imagePath),
                        fit: BoxFit.cover,
                      ),
                      Positioned(
                        top: 15,
                        right: 15,
                        child: CircularIconButton(
                          splashColor: Colors.black.withOpacity(0.5),
                          icon: const Icon(
                            Ionicons.close_sharp,
                            color: Colors.white,
                            size: 22,
                          ),
                          onTap: () {
                            Get.back();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                color: Colors.white,
                height: 70,
                child: Row(children: [
                  Container(
                    margin: EdgeInsets.only(left: 20, right: 20),
                    decoration: BoxDecoration(
                        color: Get.theme.colorScheme.primary,
                        shape: BoxShape.circle),
                    child: IconButton(
                        onPressed: () {}, icon: Icon(Icons.download)),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Get.theme.colorScheme.primary,
                        shape: BoxShape.circle),
                    child:
                        IconButton(onPressed: () {}, icon: Icon(Icons.share)),
                  ),
                  Expanded(child: SizedBox()),
                  Container(
                    margin: EdgeInsets.only(right: 25),
                    decoration: BoxDecoration(
                        color: Get.theme.colorScheme.primary,
                        shape: BoxShape.circle),
                    child: IconButton(
                        onPressed: () {
                          widget.onSend();
                        },
                        icon: Icon(Icons.send)),
                  ),
                ]),
              )
            ],
          )),
    );
  }
}
