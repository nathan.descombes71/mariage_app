import 'package:get/get.dart';

import 'camera_screen_view_controller.dart';

class CameraScreenViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => CameraScreenViewController(),
    );
  }
}
