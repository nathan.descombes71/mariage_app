import 'dart:math';

import 'package:camera/camera.dart';
import 'package:ionicons/ionicons.dart';
import 'package:mariage_app/presentation/core/scaffold/n_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mariage_app/presentation/navigation/routes.dart';

import '../../core/widgets/circular_icon_button.dart';
import '../../core/widgets/zoomable_widget.dart';
import 'camera_screen_view_controller.dart';

class CameraScreenView extends GetView<CameraScreenViewController> {
  CameraScreenView({Key? key}) : super(key: key);

  Widget emptyContainer() {
    return Container(
      alignment: Alignment.center,
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          const Text(
            "Aucune caméra trouvé",
            style: TextStyle(
              fontSize: 16.0,
              color: Colors.black,
            ),
          ),
          TextButton(
            onPressed: () {},
            child: const Text(
              "Retourner au menu",
              style: TextStyle(
                fontSize: 16.0,
                color: Colors.black,
              ),
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return controller.obx(
      (state) {
        return Scaffold(
          body: Stack(
            children: <Widget>[
              ClipRRect(
                child: OverflowBox(
                    child: ZoomableWidget(
                        showZoom: false,
                        child: CameraPreview(controller.cameraController),
                        onTapUp: (scaledPoint) {
                          controller.cameraController
                              .setFocusPoint(scaledPoint);
                        },
                        onZoom: (zoom) {
                          // print(zoom.);
                          if (zoom > controller.minAvailableZoom &&
                              zoom < controller.maxAvailableZoom) {
                            controller.currentZoomLevel.value = zoom;

                            controller.cameraController.setZoomLevel(zoom);
                          }
                        })),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: CircularIconButton(
                    splashColor: Colors.black.withOpacity(0.5),
                    icon: const Icon(
                      Ionicons.settings_outline,
                      color: Colors.white,
                      size: 22,
                    ),
                    onTap: () {
                      Get.offAndToNamed(Routes.LOGIN);
                    },
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: CircularIconButton(
                    splashColor: Colors.black.withOpacity(0.5),
                    icon: Icon(
                      controller.rotateCameraStateManager.current().icon,
                      color: Colors.white,
                      size: 22,
                    ),
                    onTap: () async {
                      controller.rotateCameraStateManager.next();
                      // await controller.cameraController.setFlashMode(controller
                      //     .rotateCameraStateManager
                      //     .current()
                      //     .flashMode);
                      controller.refresh();
                    },
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        children: [
                          Expanded(
                            child: Obx(() => Slider(
                                  value: controller.currentZoomLevel.value,
                                  min: controller.minAvailableZoom,
                                  max: controller.maxAvailableZoom,
                                  activeColor: Colors.white,
                                  inactiveColor: Colors.white30,
                                  onChanged: (value) async {
                                    controller.currentZoomLevel.value = value;
                                    await controller.cameraController
                                        .setZoomLevel(value);
                                  },
                                )),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              color: Colors.black87,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                controller.currentZoomLevel.toStringAsFixed(1) +
                                    'x',
                                style: const TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 120.0,
                      padding: const EdgeInsets.all(20.0),
                      color: Colors.black45,
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(50.0)),
                                onTap: () async {
                                  await controller.captureImage();
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Image.asset(
                                    'assets/images/shutter.png',
                                    width: 72.0,
                                    height: 72.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(50.0)),
                                onTap: () async {
                                  if (!controller.toggleCamera.value) {
                                    await controller.onCameraSelected(
                                        controller.cameras[1]);
                                    controller.toggleCamera.value = true;
                                  } else {
                                    await controller.onCameraSelected(
                                        controller.cameras[0]);
                                    controller.toggleCamera.value = false;
                                  }
                                  controller.refresh();
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Image.asset(
                                    'assets/images/switch_camera.png',
                                    color: Colors.grey[200],
                                    width: 42.0,
                                    height: 42.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(50.0)),
                                onTap: () async {
                                  await controller.getGalleryImage();
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Image.asset(
                                    'assets/images/gallery_button.png',
                                    color: Colors.grey[200],
                                    width: 42.0,
                                    height: 42.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
