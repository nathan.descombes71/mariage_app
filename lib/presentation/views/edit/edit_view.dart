import 'package:mariage_app/presentation/core/scaffold/n_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'edit_view_controller.dart';

class EditView extends GetView<EditViewController> {
  EditView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => Scaffold(
        backgroundColor: Colors.black,
        body: SizedBox(
            height: Get.height,
            width: Get.width,
            child: Obx(
              () => Image.network(
                controller.allPhotos[controller.index].value.urlPhoto,
              ),
            )
            //  Wrap(
            //   children: List.generate(
            //     controller.allPhotos.length,
            //     (index) => Image.network(
            //       controller.allPhotos[index].urlPhoto,
            //       height: 200,
            //     ),
            //   ),
            // ),
            )));
  }
}
