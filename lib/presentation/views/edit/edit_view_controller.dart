import 'dart:async';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
// import 'dart:html';

import '../../../domain/feature/restaurant/entities/photo.dart';

class EditViewController extends GetxController with StateMixin {
  EditViewController();

  List<Rx<Photo>> allPhotos = [];

  int index = 0;

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());

    Reference ref = FirebaseStorage.instance.ref();

    ListResult listResult = await ref.listAll();

    //  print((await listResult.items.first.getDownloadURL()));

    for (var onePhoto in listResult.items) {
      Photo photoToAdd = Photo(
          nbPassage: 0,
          photopgraph:
              (await onePhoto.getMetadata()).customMetadata?["user"] ?? "",
          urlPhoto: await onePhoto.getDownloadURL(),
          id: (await onePhoto.getMetadata()).customMetadata?["id"] ?? "");

      allPhotos.add(photoToAdd.obs);
    }

    print(allPhotos.length);

    Timer.periodic(Duration(seconds: 7), (timer) {
      if ((index + 1) <= allPhotos.length) {
        allPhotos[index].value.nbPassage++;
      } else {
        index = 0;
      }
      refresh();
      index++;
    });

    Timer.periodic(Duration(seconds: 18), (timer) async {
      List<String> idsPhotos = [];

      for (var onePhoto in allPhotos) {
        idsPhotos.add(onePhoto.value.id);
      }

      Reference ref = FirebaseStorage.instance.ref();

      ListResult listResult = await ref.listAll();

      //  print((await listResult.items.first.getDownloadURL()));

      for (var onePhoto in listResult.items) {
        if (!idsPhotos.contains(
            (await onePhoto.getMetadata()).customMetadata?["id"] ?? "")) {
          Photo photoToAdd = Photo(
              nbPassage: 0,
              photopgraph:
                  (await onePhoto.getMetadata()).customMetadata?["user"] ?? "",
              urlPhoto: await onePhoto.getDownloadURL(),
              id: (await onePhoto.getMetadata()).customMetadata?["id"] ?? "");

          allPhotos.add(photoToAdd.obs);
        }
      }

      allPhotos.sort((a, b) {
        return a.value.nbPassage.compareTo(b.value.nbPassage);
      });

      print(allPhotos.length);
      print("DEUXIEME ELEMENT N°PASSAGE:" +
          allPhotos[1].value.nbPassage.toString());
      print("PREMIER ELEMENT N°PASSAGE: " +
          allPhotos.first.value.nbPassage.toString());
      print("ID PREMIER ELEMENT : " + allPhotos.first.value.id);
      print("DERNIER ELEMENT N°PASSAGE: " +
          allPhotos.last.value.nbPassage.toString());

      index = 0;
    });

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  // void goFullScreen() {
  //   document.documentElement?.requestFullscreen();
  // }
}
