import 'package:get/get.dart';

import 'edit_view_controller.dart';

class EditViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => EditViewController(),
    );
  }
}
