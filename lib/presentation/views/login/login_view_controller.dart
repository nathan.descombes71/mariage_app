import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mariage_app/presentation/navigation/routes.dart';

class LoginViewController extends GetxController with StateMixin {
  LoginViewController();

  final box = GetStorage();

  String firstname = "";

  String lastname = "";

  @override
  void onInit() async {
    change(null, status: RxStatus.loading());

    super.onInit();
  }

  @override
  void onReady() async {
    change(state, status: RxStatus.success());
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  @override
  void refresh() {
    super.refresh();
  }

  saveUser() {
    box.write("nomPrenom", firstname + " " + lastname);

    Get.offAndToNamed(Routes.CAMERASCREEN);
  }
}
