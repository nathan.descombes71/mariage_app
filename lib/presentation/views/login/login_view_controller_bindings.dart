import 'package:get/get.dart';

import 'login_view_controller.dart';

class LoginViewControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(
      () => LoginViewController(),
    );
  }
  
}
