import 'package:mariage_app/presentation/core/scaffold/n_scaffold.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../core/utils.dart';
import '../../navigation/routes.dart';
import 'login_view_controller.dart';

class LoginView extends GetView<LoginViewController> {
  LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => Scaffold(
        backgroundColor: Get.theme.colorScheme.background,
        body: Container(
          height: Get.height,
          width: Get.width,
          child: Stack(
            children: [
              Positioned(
                  left: 0,
                  bottom: 0,
                  child: Image.asset("assets/graph/botleftlogin.png")),
              Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                        width: Get.width * 0.7,
                        child: TextFormField(
                          onChanged: (value) {
                            controller.firstname = value;
                          },
                          cursorColor: Colors.black,
                          decoration:
                              InputDecoration(hintText: "Entrez votre prénom"),
                        )),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                        width: Get.width * 0.7,
                        child: TextFormField(
                          onChanged: (value) {
                            controller.lastname = value;
                          },
                          cursorColor: Colors.black,
                          decoration: InputDecoration(
                              hintText: "Entrez votre nom de famille"),
                        )),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      width: Get.width * 0.7,
                      child: ElevatedButton(
                          style: buttonStyleApp(),
                          onPressed: () {
                            controller.saveUser();
                          },
                          child: Text("Se connecter")),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      width: Get.width * 0.7,
                      child: ElevatedButton(
                          style: buttonStyleApp(),
                          onPressed: () {
                            // controller.saveUser();
                            Get.toNamed(Routes.EDIT);
                          },
                          child: Text("Vue admin")),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )));
  }

  

  // OutlineInputBorder textfieldOutline() {
  //   return OutlineInputBorder(
  //       borderSide:
  //           BorderSide(color: Get.theme.colorScheme.primary, width: 2.0),
  //       borderRadius: BorderRadius.all(Radius.circular(15)));
  // }
}
