// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'menus_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MenusDto _$MenusDtoFromJson(Map<String, dynamic> json) {
  return _MenusDto.fromJson(json);
}

/// @nodoc
class _$MenusDtoTearOff {
  const _$MenusDtoTearOff();

  _MenusDto call(
      {@JsonKey(name: 'prix', includeIfNull: false) required double prix,
      @JsonKey(name: 'photoPath') String? photoPath,
      @JsonKey(name: 'name') String? name,
      @JsonKey(name: 'description') String? description,
      @JsonKey(name: 'dessert') String? dessert,
      @JsonKey(name: 'entree') String? entree,
      @JsonKey(name: 'plat') String? plat,
      @JsonKey(name: 'boisson') String? boisson}) {
    return _MenusDto(
      prix: prix,
      photoPath: photoPath,
      name: name,
      description: description,
      dessert: dessert,
      entree: entree,
      plat: plat,
      boisson: boisson,
    );
  }

  MenusDto fromJson(Map<String, Object?> json) {
    return MenusDto.fromJson(json);
  }
}

/// @nodoc
const $MenusDto = _$MenusDtoTearOff();

/// @nodoc
mixin _$MenusDto {
  @JsonKey(name: 'prix', includeIfNull: false)
  double get prix => throw _privateConstructorUsedError;
  @JsonKey(name: 'photoPath')
  String? get photoPath => throw _privateConstructorUsedError;
  @JsonKey(name: 'name')
  String? get name => throw _privateConstructorUsedError;
  @JsonKey(name: 'description')
  String? get description => throw _privateConstructorUsedError;
  @JsonKey(name: 'dessert')
  String? get dessert => throw _privateConstructorUsedError;
  @JsonKey(name: 'entree')
  String? get entree => throw _privateConstructorUsedError;
  @JsonKey(name: 'plat')
  String? get plat => throw _privateConstructorUsedError;
  @JsonKey(name: 'boisson')
  String? get boisson => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MenusDtoCopyWith<MenusDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MenusDtoCopyWith<$Res> {
  factory $MenusDtoCopyWith(MenusDto value, $Res Function(MenusDto) then) =
      _$MenusDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'prix', includeIfNull: false) double prix,
      @JsonKey(name: 'photoPath') String? photoPath,
      @JsonKey(name: 'name') String? name,
      @JsonKey(name: 'description') String? description,
      @JsonKey(name: 'dessert') String? dessert,
      @JsonKey(name: 'entree') String? entree,
      @JsonKey(name: 'plat') String? plat,
      @JsonKey(name: 'boisson') String? boisson});
}

/// @nodoc
class _$MenusDtoCopyWithImpl<$Res> implements $MenusDtoCopyWith<$Res> {
  _$MenusDtoCopyWithImpl(this._value, this._then);

  final MenusDto _value;
  // ignore: unused_field
  final $Res Function(MenusDto) _then;

  @override
  $Res call({
    Object? prix = freezed,
    Object? photoPath = freezed,
    Object? name = freezed,
    Object? description = freezed,
    Object? dessert = freezed,
    Object? entree = freezed,
    Object? plat = freezed,
    Object? boisson = freezed,
  }) {
    return _then(_value.copyWith(
      prix: prix == freezed
          ? _value.prix
          : prix // ignore: cast_nullable_to_non_nullable
              as double,
      photoPath: photoPath == freezed
          ? _value.photoPath
          : photoPath // ignore: cast_nullable_to_non_nullable
              as String?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      dessert: dessert == freezed
          ? _value.dessert
          : dessert // ignore: cast_nullable_to_non_nullable
              as String?,
      entree: entree == freezed
          ? _value.entree
          : entree // ignore: cast_nullable_to_non_nullable
              as String?,
      plat: plat == freezed
          ? _value.plat
          : plat // ignore: cast_nullable_to_non_nullable
              as String?,
      boisson: boisson == freezed
          ? _value.boisson
          : boisson // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$MenusDtoCopyWith<$Res> implements $MenusDtoCopyWith<$Res> {
  factory _$MenusDtoCopyWith(_MenusDto value, $Res Function(_MenusDto) then) =
      __$MenusDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'prix', includeIfNull: false) double prix,
      @JsonKey(name: 'photoPath') String? photoPath,
      @JsonKey(name: 'name') String? name,
      @JsonKey(name: 'description') String? description,
      @JsonKey(name: 'dessert') String? dessert,
      @JsonKey(name: 'entree') String? entree,
      @JsonKey(name: 'plat') String? plat,
      @JsonKey(name: 'boisson') String? boisson});
}

/// @nodoc
class __$MenusDtoCopyWithImpl<$Res> extends _$MenusDtoCopyWithImpl<$Res>
    implements _$MenusDtoCopyWith<$Res> {
  __$MenusDtoCopyWithImpl(_MenusDto _value, $Res Function(_MenusDto) _then)
      : super(_value, (v) => _then(v as _MenusDto));

  @override
  _MenusDto get _value => super._value as _MenusDto;

  @override
  $Res call({
    Object? prix = freezed,
    Object? photoPath = freezed,
    Object? name = freezed,
    Object? description = freezed,
    Object? dessert = freezed,
    Object? entree = freezed,
    Object? plat = freezed,
    Object? boisson = freezed,
  }) {
    return _then(_MenusDto(
      prix: prix == freezed
          ? _value.prix
          : prix // ignore: cast_nullable_to_non_nullable
              as double,
      photoPath: photoPath == freezed
          ? _value.photoPath
          : photoPath // ignore: cast_nullable_to_non_nullable
              as String?,
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      description: description == freezed
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String?,
      dessert: dessert == freezed
          ? _value.dessert
          : dessert // ignore: cast_nullable_to_non_nullable
              as String?,
      entree: entree == freezed
          ? _value.entree
          : entree // ignore: cast_nullable_to_non_nullable
              as String?,
      plat: plat == freezed
          ? _value.plat
          : plat // ignore: cast_nullable_to_non_nullable
              as String?,
      boisson: boisson == freezed
          ? _value.boisson
          : boisson // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MenusDto implements _MenusDto {
  const _$_MenusDto(
      {@JsonKey(name: 'prix', includeIfNull: false) required this.prix,
      @JsonKey(name: 'photoPath') this.photoPath,
      @JsonKey(name: 'name') this.name,
      @JsonKey(name: 'description') this.description,
      @JsonKey(name: 'dessert') this.dessert,
      @JsonKey(name: 'entree') this.entree,
      @JsonKey(name: 'plat') this.plat,
      @JsonKey(name: 'boisson') this.boisson});

  factory _$_MenusDto.fromJson(Map<String, dynamic> json) =>
      _$$_MenusDtoFromJson(json);

  @override
  @JsonKey(name: 'prix', includeIfNull: false)
  final double prix;
  @override
  @JsonKey(name: 'photoPath')
  final String? photoPath;
  @override
  @JsonKey(name: 'name')
  final String? name;
  @override
  @JsonKey(name: 'description')
  final String? description;
  @override
  @JsonKey(name: 'dessert')
  final String? dessert;
  @override
  @JsonKey(name: 'entree')
  final String? entree;
  @override
  @JsonKey(name: 'plat')
  final String? plat;
  @override
  @JsonKey(name: 'boisson')
  final String? boisson;

  @override
  String toString() {
    return 'MenusDto(prix: $prix, photoPath: $photoPath, name: $name, description: $description, dessert: $dessert, entree: $entree, plat: $plat, boisson: $boisson)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _MenusDto &&
            const DeepCollectionEquality().equals(other.prix, prix) &&
            const DeepCollectionEquality().equals(other.photoPath, photoPath) &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality()
                .equals(other.description, description) &&
            const DeepCollectionEquality().equals(other.dessert, dessert) &&
            const DeepCollectionEquality().equals(other.entree, entree) &&
            const DeepCollectionEquality().equals(other.plat, plat) &&
            const DeepCollectionEquality().equals(other.boisson, boisson));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(prix),
      const DeepCollectionEquality().hash(photoPath),
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(description),
      const DeepCollectionEquality().hash(dessert),
      const DeepCollectionEquality().hash(entree),
      const DeepCollectionEquality().hash(plat),
      const DeepCollectionEquality().hash(boisson));

  @JsonKey(ignore: true)
  @override
  _$MenusDtoCopyWith<_MenusDto> get copyWith =>
      __$MenusDtoCopyWithImpl<_MenusDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MenusDtoToJson(this);
  }
}

abstract class _MenusDto implements MenusDto {
  const factory _MenusDto(
      {@JsonKey(name: 'prix', includeIfNull: false) required double prix,
      @JsonKey(name: 'photoPath') String? photoPath,
      @JsonKey(name: 'name') String? name,
      @JsonKey(name: 'description') String? description,
      @JsonKey(name: 'dessert') String? dessert,
      @JsonKey(name: 'entree') String? entree,
      @JsonKey(name: 'plat') String? plat,
      @JsonKey(name: 'boisson') String? boisson}) = _$_MenusDto;

  factory _MenusDto.fromJson(Map<String, dynamic> json) = _$_MenusDto.fromJson;

  @override
  @JsonKey(name: 'prix', includeIfNull: false)
  double get prix;
  @override
  @JsonKey(name: 'photoPath')
  String? get photoPath;
  @override
  @JsonKey(name: 'name')
  String? get name;
  @override
  @JsonKey(name: 'description')
  String? get description;
  @override
  @JsonKey(name: 'dessert')
  String? get dessert;
  @override
  @JsonKey(name: 'entree')
  String? get entree;
  @override
  @JsonKey(name: 'plat')
  String? get plat;
  @override
  @JsonKey(name: 'boisson')
  String? get boisson;
  @override
  @JsonKey(ignore: true)
  _$MenusDtoCopyWith<_MenusDto> get copyWith =>
      throw _privateConstructorUsedError;
}
