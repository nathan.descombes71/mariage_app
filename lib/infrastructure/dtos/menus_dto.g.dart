// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'menus_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_MenusDto _$$_MenusDtoFromJson(Map<String, dynamic> json) => _$_MenusDto(
      prix: (json['prix'] as num).toDouble(),
      photoPath: json['photoPath'] as String?,
      name: json['name'] as String?,
      description: json['description'] as String?,
      dessert: json['dessert'] as String?,
      entree: json['entree'] as String?,
      plat: json['plat'] as String?,
      boisson: json['boisson'] as String?,
    );

Map<String, dynamic> _$$_MenusDtoToJson(_$_MenusDto instance) =>
    <String, dynamic>{
      'prix': instance.prix,
      'photoPath': instance.photoPath,
      'name': instance.name,
      'description': instance.description,
      'dessert': instance.dessert,
      'entree': instance.entree,
      'plat': instance.plat,
      'boisson': instance.boisson,
    };
