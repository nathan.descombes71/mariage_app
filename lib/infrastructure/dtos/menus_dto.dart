
import 'package:mariage_app/infrastructure/dtos/serialization_normalizer.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'menus_dto.freezed.dart';
part 'menus_dto.g.dart';

@freezed
abstract class MenusDto with _$MenusDto {
  const factory MenusDto({
    @JsonKey(name: 'prix', includeIfNull: false) required double prix,
    @JsonKey(name: 'photoPath') String? photoPath,
    @JsonKey(name: 'name') String? name,
    @JsonKey(name: 'description') String? description,
    @JsonKey(name: 'dessert') String? dessert,
    @JsonKey(name: 'entree') String? entree,
    @JsonKey(name: 'plat') String? plat,
    @JsonKey(name: 'boisson') String? boisson,
  }) = _MenusDto;
  factory MenusDto.fromJson(Map<String, dynamic> json) =>
      _$MenusDtoFromJson(json);
}

extension OnMenusJson on Map<String, dynamic> {
  MenusDto get toMenusDto {
    return MenusDto.fromJson(this);
  }
}

extension OnListMenusJson on List<Map<String, dynamic>> {
  List<MenusDto> get toMenusDto {
    return map<MenusDto>((Map<String, dynamic> e) => e.toMenusDto).toList();
  }
}
