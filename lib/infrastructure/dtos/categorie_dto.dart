
import 'package:mariage_app/infrastructure/dtos/menus_dto.dart';
import 'package:mariage_app/infrastructure/dtos/serialization_normalizer.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'categorie_dto.freezed.dart';
part 'categorie_dto.g.dart';

@freezed
abstract class CategorieDto with _$CategorieDto {
  const factory CategorieDto({
    @JsonKey(name: 'name', includeIfNull: false) required String name,
    @JsonKey(name: 'listMenus') List<MenusDto>? listMenus,

  }) = _CategorieDto;
  factory CategorieDto.fromJson(Map<String, dynamic> json) =>
      _$CategorieDtoFromJson(json);
}

extension OnCategorieJson on Map<String, dynamic> {
  CategorieDto get toCategorieDto {
    return CategorieDto.fromJson(this);
  }
}

extension OnListCategorieJson on List<Map<String, dynamic>> {
  List<CategorieDto> get toCategorieDto {
    return map<CategorieDto>((Map<String, dynamic> e) => e.toCategorieDto).toList();
  }
}
