// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'categorie_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CategorieDto _$$_CategorieDtoFromJson(Map<String, dynamic> json) =>
    _$_CategorieDto(
      name: json['name'] as String,
      listMenus: (json['listMenus'] as List<dynamic>?)
          ?.map((e) => MenusDto.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_CategorieDtoToJson(_$_CategorieDto instance) =>
    <String, dynamic>{
      'name': instance.name,
      'listMenus': instance.listMenus,
    };
