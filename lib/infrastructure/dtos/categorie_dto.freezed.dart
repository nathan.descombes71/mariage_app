// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'categorie_dto.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CategorieDto _$CategorieDtoFromJson(Map<String, dynamic> json) {
  return _CategorieDto.fromJson(json);
}

/// @nodoc
class _$CategorieDtoTearOff {
  const _$CategorieDtoTearOff();

  _CategorieDto call(
      {@JsonKey(name: 'name', includeIfNull: false) required String name,
      @JsonKey(name: 'listMenus') List<MenusDto>? listMenus}) {
    return _CategorieDto(
      name: name,
      listMenus: listMenus,
    );
  }

  CategorieDto fromJson(Map<String, Object?> json) {
    return CategorieDto.fromJson(json);
  }
}

/// @nodoc
const $CategorieDto = _$CategorieDtoTearOff();

/// @nodoc
mixin _$CategorieDto {
  @JsonKey(name: 'name', includeIfNull: false)
  String get name => throw _privateConstructorUsedError;
  @JsonKey(name: 'listMenus')
  List<MenusDto>? get listMenus => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CategorieDtoCopyWith<CategorieDto> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CategorieDtoCopyWith<$Res> {
  factory $CategorieDtoCopyWith(
          CategorieDto value, $Res Function(CategorieDto) then) =
      _$CategorieDtoCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'name', includeIfNull: false) String name,
      @JsonKey(name: 'listMenus') List<MenusDto>? listMenus});
}

/// @nodoc
class _$CategorieDtoCopyWithImpl<$Res> implements $CategorieDtoCopyWith<$Res> {
  _$CategorieDtoCopyWithImpl(this._value, this._then);

  final CategorieDto _value;
  // ignore: unused_field
  final $Res Function(CategorieDto) _then;

  @override
  $Res call({
    Object? name = freezed,
    Object? listMenus = freezed,
  }) {
    return _then(_value.copyWith(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      listMenus: listMenus == freezed
          ? _value.listMenus
          : listMenus // ignore: cast_nullable_to_non_nullable
              as List<MenusDto>?,
    ));
  }
}

/// @nodoc
abstract class _$CategorieDtoCopyWith<$Res>
    implements $CategorieDtoCopyWith<$Res> {
  factory _$CategorieDtoCopyWith(
          _CategorieDto value, $Res Function(_CategorieDto) then) =
      __$CategorieDtoCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'name', includeIfNull: false) String name,
      @JsonKey(name: 'listMenus') List<MenusDto>? listMenus});
}

/// @nodoc
class __$CategorieDtoCopyWithImpl<$Res> extends _$CategorieDtoCopyWithImpl<$Res>
    implements _$CategorieDtoCopyWith<$Res> {
  __$CategorieDtoCopyWithImpl(
      _CategorieDto _value, $Res Function(_CategorieDto) _then)
      : super(_value, (v) => _then(v as _CategorieDto));

  @override
  _CategorieDto get _value => super._value as _CategorieDto;

  @override
  $Res call({
    Object? name = freezed,
    Object? listMenus = freezed,
  }) {
    return _then(_CategorieDto(
      name: name == freezed
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      listMenus: listMenus == freezed
          ? _value.listMenus
          : listMenus // ignore: cast_nullable_to_non_nullable
              as List<MenusDto>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CategorieDto implements _CategorieDto {
  const _$_CategorieDto(
      {@JsonKey(name: 'name', includeIfNull: false) required this.name,
      @JsonKey(name: 'listMenus') this.listMenus});

  factory _$_CategorieDto.fromJson(Map<String, dynamic> json) =>
      _$$_CategorieDtoFromJson(json);

  @override
  @JsonKey(name: 'name', includeIfNull: false)
  final String name;
  @override
  @JsonKey(name: 'listMenus')
  final List<MenusDto>? listMenus;

  @override
  String toString() {
    return 'CategorieDto(name: $name, listMenus: $listMenus)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _CategorieDto &&
            const DeepCollectionEquality().equals(other.name, name) &&
            const DeepCollectionEquality().equals(other.listMenus, listMenus));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(name),
      const DeepCollectionEquality().hash(listMenus));

  @JsonKey(ignore: true)
  @override
  _$CategorieDtoCopyWith<_CategorieDto> get copyWith =>
      __$CategorieDtoCopyWithImpl<_CategorieDto>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CategorieDtoToJson(this);
  }
}

abstract class _CategorieDto implements CategorieDto {
  const factory _CategorieDto(
      {@JsonKey(name: 'name', includeIfNull: false) required String name,
      @JsonKey(name: 'listMenus') List<MenusDto>? listMenus}) = _$_CategorieDto;

  factory _CategorieDto.fromJson(Map<String, dynamic> json) =
      _$_CategorieDto.fromJson;

  @override
  @JsonKey(name: 'name', includeIfNull: false)
  String get name;
  @override
  @JsonKey(name: 'listMenus')
  List<MenusDto>? get listMenus;
  @override
  @JsonKey(ignore: true)
  _$CategorieDtoCopyWith<_CategorieDto> get copyWith =>
      throw _privateConstructorUsedError;
}
