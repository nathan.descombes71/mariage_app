import 'package:get/get.dart';

// import '../environment/env.dart';
import 'rest_api_interceptor.dart';

class RestApiClient extends GetConnect {
  final GetHttpClient client = GetHttpClient();

  RestApiClient({
    RestApiInterceptor? restApiInterceptor,
  }) {
    client.baseUrl = "http://localhost:5001/restocard/us-central1/v1";
    client.timeout = Duration(seconds: 15);
    client.addRequestModifier<Object?>(
        (request) => restApiInterceptor!.requestModifier(request));
    client.addResponseModifier((request, response) =>
        restApiInterceptor!.responseModifier(request, response));
  }
}
