import 'package:uuid/uuid.dart';

class Photo {
  String urlPhoto;
  String photopgraph;
  int nbPassage;
  String id;

  Photo({
    required this.photopgraph,
    required this.urlPhoto,
    required this.nbPassage,
    required this.id,
  });
}

extension OnPhoto on Photo {
  Photo copyWith({
    String? photopgraph,
    String? urlPhoto,
    int? nbPassage,
    String? id,
  }) {
    return Photo(
      urlPhoto: urlPhoto ?? this.urlPhoto,
      photopgraph: photopgraph ?? this.photopgraph,
      nbPassage: nbPassage ?? this.nbPassage,
      id: id ?? this.id,
    );
  }

  // PhotoDto get toDto {
  //   return PhotoDto(
  //     id: id,
  //     name: name,
  //     createdAt: createdAt,
  //     imagePannelPath: imagePannelPath,
  //     listCategories: listCategories.toDto,
  //     logoPath: logoPath,
  //     textPannel: textPannel,
  //   );
  // }
}

// extension OnListPhoto on List<Photo> {
//   List<PhotoDto> get toDto {
//     List<PhotoDto> PhotoDtoList = [];

//     this.forEach((entity) => PhotoDtoList.add(entity.toDto));
//     return PhotoDtoList;
//   }
// }

// extension OnListPhotoDto on List<PhotoDto> {
//   List<Photo> get toEntity {
//     List<Photo> PhotoList = [];

//     this.forEach((dto) => PhotoList.add(dto.toEntity));
//     return PhotoList;
//   }
// }

// extension OnPhotoDto on PhotoDto {
//   Photo get toEntity {
//     return Photo(
//       id: id,
//       name: name,
//       createdAt: createdAt,
//       imagePannelPath: imagePannelPath,
//       listCategories: listCategories!.toEntity,
//       logoPath: logoPath,
//       textPannel: textPannel,
//     );
//   }
// }
